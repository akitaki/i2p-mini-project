SRC_LIST = $(wildcard src/*.c)
SRC_LIST_ND = $(notdir $(SRC_LIST))
OBJ_LIST = $(patsubst src/%.c,build/%.o,$(SRC_LIST))

DEPS = allegro-5 allegro_font-5 allegro_image-5 allegro_ttf-5 allegro_audio-5 allegro_primitives-5 allegro_acodec-5

CFLAGS = -std=c11
LDFLAGS = -L${LD_LIBRARY_PATH} `pkg-config --libs --cflags $(DEPS)` -lm

GREEN = \033[0;32m
NC = \033[0m

all: clean createdirs game copyassets

debug: DEBUG = -g -DDEBUG=1
debug: all

game: $(OBJ_LIST)
	@echo -e '${GREEN}Linking${NC}'
	$(CC) -o bin/game $(OBJ_LIST) $(LDFLAGS) $(DEBUG)

build/%.o: src/%.c
	@echo -e '${GREEN}Building $<${NC}'
	$(CC) -c $< -o $@ $(CFLAGS) $(DEBUG)

createdirs:
	@echo -e '${GREEN}Creating directories${NC}'
	mkdir -p build bin

copyassets:
	@echo -e '${GREEN}Copying assets${NC}'
	cp -r assets/ bin/

clean:
	@echo -e '${GREEN}Cleaning${NC}'
	rm -rf build/ bin/

lldb: debug
	cd bin/ && lldb game

run: all
	cd bin/ && ./game

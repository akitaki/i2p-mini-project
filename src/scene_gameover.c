#include "scene_gameover.h"
#include "game.h"
#include "scene_menu.h"
#include "shared.h"
#include "utility.h"
#include "button.h"
#include "data_store.h"
#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_primitives.h>
#include <math.h>
#include <stdio.h>
#include <string.h>

static PlayerData saved_pd;
static Button main_menu_btn;

static bool is_new_hi;

static void init() {
    main_menu_btn = new_button(SCREEN_W - 210, SCREEN_H - 48, 200, 38, img_main_menu, img_main_menu_pressed);
    game_log("Gameover scene initialized");
}

static void update() {}

static void on_mouse_down(int btn, int x, int y, int dz) {
    if (btn == 1) {
        bool in_rect = pnt_in_rect(mouse_x, mouse_y, SCREEN_W - 210,
                                   SCREEN_H - 48, 200, 38);
        if (in_rect)
            game_change_scene(scene_menu_create());
    }
}

static void draw_statistics() {
    static char text[100];

    draw_grey_bg_box();

    int y_cur = 105;
    sprintf(text, "Score: %03d pt", saved_pd.score);
    al_draw_text(font_pirulen_24, al_map_rgb(255, 255, 255), SCREEN_W / 2,
                 y_cur, ALLEGRO_ALIGN_CENTER, text);
    y_cur += 60;

    sprintf(text, "Kills: %03d", saved_pd.kills);
    al_draw_text(font_pirulen_24, al_map_rgb(255, 255, 255), SCREEN_W / 2,
                 y_cur, ALLEGRO_ALIGN_CENTER, text);
    y_cur += 60;

    sprintf(text, "Survived Time: %.2lf s",
            saved_pd.end_time - saved_pd.start_time);
    al_draw_text(font_pirulen_24, al_map_rgb(255, 255, 255), SCREEN_W / 2,
                 y_cur, ALLEGRO_ALIGN_CENTER, text);
    y_cur += 60;

    float accuracy =
        saved_pd.bullet_count == 0
            ? 0.0f
            : (float)saved_pd.hit_count / saved_pd.bullet_count * 100.0f;
    sprintf(text, "Accuracy: %6.2f %%", accuracy);
    al_draw_text(font_pirulen_24, al_map_rgb(255, 255, 255), SCREEN_W / 2,
                 y_cur, ALLEGRO_ALIGN_CENTER, text);
    y_cur += 75;

    if (is_new_hi) {
        sprintf(text, "New High Score!!!");
        al_draw_text(font_pirulen_24, al_map_rgb(0xff, 0xff, 0x00),
                     SCREEN_W / 2, y_cur, ALLEGRO_ALIGN_CENTER, text);
    } else {
        sprintf(text, "Highest score: %03d", get_hi_score());
        al_draw_text(font_pirulen_24, al_map_rgb(0xff, 0xff, 0xff),
                     SCREEN_W / 2, y_cur, ALLEGRO_ALIGN_CENTER, text);
    }
}

static void draw() {
    al_draw_bitmap(img_gameover_background, 0, 0, 0);

    draw_title("Game Over");
    draw_statistics();
    draw_button(&main_menu_btn);
}

static void on_key_down() { printf("gameover keydown"); }

static ALLEGRO_SAMPLE_ID bgm_id;

static void destroy() {
    stop_bgm(bgm_id);
}

Scene scene_gameover_create(PlayerData pd) {
    saved_pd = pd;
    
    is_new_hi = pd.score > get_hi_score();
    set_hi_score(pd.score);

    Scene scene;
    memset(&scene, 0, sizeof(Scene));
    scene.name          = "Start";
    scene.initialize    = &init;
    scene.update        = &update;
    scene.draw          = &draw;
    scene.destroy       = &destroy;
    scene.on_key_down   = &on_key_down;
    scene.on_mouse_down = &on_mouse_down;
    game_log("Start scene created");
    
    bgm_id = play_bgm(over_bgm, get_volume());

    return scene;
}

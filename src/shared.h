#ifndef SCENE_SHARED_H
#define SCENE_SHARED_H

#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_font.h>

extern ALLEGRO_FONT* font_pirulen_32;
extern ALLEGRO_FONT* font_pirulen_24;
ALLEGRO_FONT* font_fmr;

void shared_init(void);
void shared_destroy(void);
bool one_out_of(int denom);
int rand_sign();
ALLEGRO_COLOR interpolate_color(ALLEGRO_COLOR ca, ALLEGRO_COLOR cb,
                                ALLEGRO_COLOR cc, float ratio);

bool muted;
float get_volume();

void draw_grey_bg_box();
void draw_title(const char* title);

ALLEGRO_BITMAP* img_background;
ALLEGRO_BITMAP* img_bullet;
ALLEGRO_BITMAP* img_enemy;
ALLEGRO_BITMAP* img_plane;
ALLEGRO_BITMAP* img_sky;
ALLEGRO_SAMPLE* game_bgm;
ALLEGRO_SAMPLE* beam_fx;
ALLEGRO_SAMPLE* explosion_fx;

ALLEGRO_BITMAP* img_main_menu;
ALLEGRO_BITMAP* img_main_menu_pressed;
ALLEGRO_BITMAP* img_gameover_background;

ALLEGRO_BITMAP* img_menu_background;
ALLEGRO_BITMAP* img_settings;
ALLEGRO_BITMAP* img_settings2;
ALLEGRO_SAMPLE* bgm;

ALLEGRO_BITMAP* img_mute;
ALLEGRO_BITMAP* img_mute_p;
ALLEGRO_BITMAP* img_unmute;
ALLEGRO_BITMAP* img_unmute_p;

ALLEGRO_SAMPLE* over_bgm;

#endif

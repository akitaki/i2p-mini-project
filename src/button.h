#ifndef BUTTON_H
#define BUTTON_H 

#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_image.h>

typedef struct {
    int xi, yi, w, h;
    ALLEGRO_BITMAP* img;
    ALLEGRO_BITMAP* img_pressed;
    void (*draw)(void);
} Button;

Button new_button(int xi, int yi, int w, int h, ALLEGRO_BITMAP* img, ALLEGRO_BITMAP* img_p);
void draw_button(Button* btn);
bool clicked(Button* btn, int e_btn);

#endif /* ifndef BUTTON_H */
